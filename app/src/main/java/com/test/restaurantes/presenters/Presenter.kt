package com.test.restaurantes.presenters

import android.content.Context
import android.content.Intent

open class Presenter<T : IPresenter.IView> : IPresenter<T> {

    var mView : T? = null

    lateinit var mContext : Context

    override fun attachView(view : T?) {
        mView = view
    }

    override fun onCreated() {}

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {}

    override fun onResume() {}

    override fun onPause() {}

    override fun onStop() {}

    override fun onDestroy() {
        mView = null
    }
}