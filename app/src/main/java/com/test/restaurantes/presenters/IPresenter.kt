package com.test.restaurantes.presenters

import android.content.Intent

interface IPresenter<T> {

    fun attachView(view: T?)

    fun onCreated()

    fun onResume()

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)

    fun onPause()

    fun onStop()

    fun onDestroy()

    interface IView {}
}