package com.test.restaurantes.presentation.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.test.restaurantes.R
import com.test.restaurantes.databinding.FragmentListBinding
import com.test.restaurantes.framework.database.RestaurantEntity
import com.test.restaurantes.presentation.adapters.RestaurantAdapter

class FragmentList: Fragment(), IListPresenter.View {

    private var presenter = ListPresenter(requireContext())
    private lateinit var binding: FragmentListBinding
    private lateinit var adapter: RestaurantAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list, container, false);
        presenter.attachView(this)
        adapter = RestaurantAdapter(presenter)
        binding.rvRestaurantes.adapter = adapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onResume()
    }

    override fun showItems(restaurantList: List<RestaurantEntity>) {
        adapter.notifyDataSetChanged()
    }

    override fun navigateToDetail(restaurantEntity: RestaurantEntity) {
        // TODO navigate to detail, pass restaurant as param
        //val action = ListFragmentDirections.navigateDetail(restaurantEntity)
        //findNavController().navigate(action)
    }

}