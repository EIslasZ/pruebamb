package com.test.restaurantes.presentation

import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.test.restaurantes.R
import com.test.restaurantes.framework.database.AppDatabase


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val database = AppDatabase.getInstance(applicationContext)
        AsyncTask.execute {
            val r = database.restaurantDao.getAllRestaurants()
            Log.d("hola", "")
        }

        // TODO set init fragment
        // add nav controller

    }
}