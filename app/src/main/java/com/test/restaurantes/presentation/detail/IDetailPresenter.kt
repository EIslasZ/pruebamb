package com.test.restaurantes.presentation.detail

import com.test.restaurantes.framework.database.RestaurantEntity
import com.test.restaurantes.presenters.IPresenter

interface IDetailPresenter : IPresenter<IDetailPresenter.View> {

    interface View: IPresenter.IView {

        fun showDetail(restaurantEntity: RestaurantEntity, rating: String)
    }
}