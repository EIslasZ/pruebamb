package com.test.restaurantes.presentation.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.test.restaurantes.R
import com.test.restaurantes.databinding.FragmentDetailsBinding
import com.test.restaurantes.framework.database.RestaurantEntity

class FragmentDetails : Fragment(), IDetailPresenter.View {

    private lateinit var presenter: DetailPresenter
    private lateinit var binding: FragmentDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_details, container, false);
        presenter = DetailPresenter(requireContext())
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
    }

    override fun showDetail(restaurantEntity: RestaurantEntity, rating: String) {
        binding.restaurantItem = restaurantEntity
        binding.rating = rating
    }
}