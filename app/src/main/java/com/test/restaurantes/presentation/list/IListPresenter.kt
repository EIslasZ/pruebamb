package com.test.restaurantes.presentation.list

import com.test.restaurantes.framework.database.RestaurantEntity
import com.test.restaurantes.presenters.IPresenter

interface IListPresenter: IPresenter<IListPresenter.View> {

    fun onItemClick(itemPosition: Int)
    fun getItemsList(): List<RestaurantEntity>

    interface View: IPresenter.IView {

        fun showItems(restaurantList: List<RestaurantEntity>)
        fun navigateToDetail(restaurantEntity: RestaurantEntity)
    }
}