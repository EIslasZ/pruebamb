package com.test.restaurantes.presentation.list

import android.content.Context
import com.test.restaurantes.framework.database.IRestaurantDao
import com.test.restaurantes.framework.database.RestaurantEntity
import com.test.restaurantes.presenters.Presenter

class ListPresenter(private var context: Context): Presenter<IListPresenter.View>(), IListPresenter {

    private lateinit var restaurantDao: IRestaurantDao
    private var restaurantItems: MutableList<RestaurantEntity> = arrayListOf()

    override fun onItemClick(itemPosition: Int) {
        mView?.navigateToDetail(restaurantItems[itemPosition])
    }

    override fun getItemsList(): List<RestaurantEntity> = restaurantItems

    override fun attachView(view: IListPresenter.View?) {
        super.attachView(view)
    }

    override fun onResume() {
        restaurantItems.addAll(restaurantDao.getAllRestaurants())
    }

}