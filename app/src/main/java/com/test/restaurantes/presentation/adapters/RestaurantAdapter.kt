package com.test.restaurantes.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.test.restaurantes.R
import com.test.restaurantes.databinding.ListItemBinding
import com.test.restaurantes.framework.database.RestaurantEntity
import com.test.restaurantes.presentation.list.ListPresenter

class RestaurantAdapter(var presenter: ListPresenter): RecyclerView.Adapter<RestaurantAdapter.ResturantViewHolder>() {

    private var restaurants: List<RestaurantEntity> = presenter.getItemsList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResturantViewHolder {
        var binding: ListItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.list_item, parent, false)
        return ResturantViewHolder(binding, presenter)
    }

    override fun onBindViewHolder(holder: ResturantViewHolder, position: Int) {
        holder.bind(restaurants[position])
    }

    override fun getItemCount(): Int = restaurants.size

    class ResturantViewHolder(binding: ListItemBinding, var presenter: ListPresenter) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var binding: ListItemBinding

        fun init() {
            binding.ivNext.setOnClickListener { presenter.onItemClick(layoutPosition) }
        }

        fun bind(restaurantItem: RestaurantEntity) {
            binding.restaurantItem = restaurantItem
            binding.rating = "Rating: ${restaurantItem.rating}"
        }
    }

}