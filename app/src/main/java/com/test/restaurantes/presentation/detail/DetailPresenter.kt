package com.test.restaurantes.presentation.detail

import android.content.Context
import com.test.restaurantes.R
import com.test.restaurantes.framework.database.RestaurantEntity
import com.test.restaurantes.presenters.Presenter

class DetailPresenter(private var context: Context): Presenter<IDetailPresenter.View>(), IDetailPresenter {

    private lateinit var detail: RestaurantEntity

    override fun attachView(view: IDetailPresenter.View?) {
        super.attachView(view)
        // TODO get detail
        // detail = detail
    }

    override fun onResume() {
        super.onResume()
        mView?.showDetail(detail, context.getString(R.string.lbl_rating_detail, detail.rating))
    }

}