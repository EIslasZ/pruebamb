# Restaurantes

![UI](https://github.com/Music-Battles/PruebaTecnicaAndroid/blob/main/ui.png)

## Instrucciones

Descarga o clona el proyecto en tu local.

Dentro del proyecto vas a encontrar:
- **RestaurantEntity.kt**: El modelo `Restaurante` con una función para poblar datos.
- **IRestaurantDao.kt**: El Data Access Object del modelo `Restaurante`.
- **AppDatabase.kt**: Este es el punto de acceso a la base de datos Room ya configurado.
- **FragmentDetails.kt**: El fragmento del detalle de un restaurante.
- **colors.xml**: Aquí viene el catálogo de colores.

Debes de armar una interfaz de acuerdo al diseño, poblar con una función que ya viene dentro de `RestaurantEntity.kt`, mostrar la data que se obtiene de la base de datos local [Room](https://developer.android.com/jetpack/androidx/releases/room) y crear un fujo de navegación entre el detalle y el listado.

## Interfaz

Puedes inspeccionar el [Design Handoff aquí](https://www.figma.com/file/FUyW4iSLevqRBOI4SwLUBZ/Restaurantes-Android?node-id=0%3A1)

## Puntos extras

* Documentación de código.
* Arquitectura limpia.
* Pruebas unitarias
* Animaciones

# Happy coding! :slightly_smiling_face: :computer: :coffee: